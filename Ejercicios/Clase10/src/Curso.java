public class Curso {

    private Alumno[] alumnos;
    private int c;
    
    public Curso() {
        alumnos = new Alumno[20];
        c = 0;
    }
    
    public void agregarAlumno(Alumno nuevo) {
        alumnos[c] = nuevo;
        c++;
    }
    
    public Alumno[] getAlumnos() {
        return alumnos;
    }
    
}
