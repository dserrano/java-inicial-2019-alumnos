package curso;

import java.util.Scanner;

/**
 *
 * @author java
 */
public class Programa {

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        Alumno []v;
        
        System.out.println("Ingrese la cantidad de alumnos:");
        int cantidad = sc.nextInt();
        int i;
        
        v = new Alumno[cantidad];
        
        for(i = 0; i < v.length; i++) {
            
            System.out.println("Ingrese el nombre: ");
            String nombre = sc.next();
            System.out.println("Ingrese el legajo: ");
            int legajo = sc.nextInt();
            System.out.println("Ingrese el promedio: ");
            float promedio = sc.nextFloat();
            
            v[i] = new Alumno(nombre, legajo, promedio);
        }
   
        // 1- Listado de alumnos
        
        System.out.println("Listado de alumnos");
        System.out.println("Nombre   Legajo    Promedio");
        for (i = 0; i < v.length; i++) {
            System.out.println(v[i].getNombre() + "   "  + v[i].getLegajo() + "   "  + v[i].getPromedio());
        }
        
        
        // 2- Promedio general del curso
        float acum = 0;
        
        for (i = 0; i < v.length; i++) {
            acum += v[i].getPromedio();
        }
        
        float promedioGeneral = acum / v.length;
        
        System.out.println("Promedio general del curso:" + promedioGeneral);
        
        // 3- Cantidad de alumnos con promedio mayor a 8
        
        int c = 0;
        
        for(i = 0; i < v.length; i++) {
            if (v[i].getPromedio() > 8)
                c++;
        }
        
        System.out.println("Cantidad de alumnos con promedio mayor a 8: " + c);
    }
    
    
}
